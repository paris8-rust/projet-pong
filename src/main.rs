use ggez;
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::nalgebra as na;
use ggez::event;
use ggez::input::keyboard::{self, KeyCode};
use rand::{self, thread_rng, Rng};

const MIDDLE_LINE_W: f32 = 2.0;
const PADDLE_HEIGHT: f32 = 100.0;
const PADDLE_WIDTH: f32 = 20.0;
const PADDLE_WIDTH_HALF: f32 = PADDLE_WIDTH * 0.5;
const PADDLE_HEIGHT_HALF: f32 = PADDLE_HEIGHT * 0.5;
const BALL_SIZE: f32 = 30.0;
const BALL_SIZE_HALF: f32 = BALL_SIZE * 0.5;
const PLAYER_SPEED: f32 = 600.0;
const BALL_SPEED: f32 = 475.0;

fn clamp(value: &mut f32, low: f32, high: f32) {
    if *value < low {
        *value = low;
    } else if *value > high {
        *value = high;
    }
}

fn move_paddle(pos: &mut na::Point2<f32>, keycode: KeyCode, y_dir: f32, ctx: &mut Context) {
    let dt = ggez::timer::delta(ctx).as_secs_f32();
    let screen_h = graphics::drawable_size(ctx).1;
    if keyboard::is_key_pressed(ctx, keycode) {
        pos.y += y_dir * PLAYER_SPEED * dt;
    }
    clamp(&mut pos.y, PADDLE_HEIGHT_HALF, screen_h-PADDLE_HEIGHT_HALF);
}

fn randomize_vec(vec: &mut na::Vector2<f32>, x: f32, y: f32) {
    let mut rng = thread_rng();
    vec.x = match rng.gen_bool(0.5) {
        true => x,
        false => -x,
    };
    vec.y = match rng.gen_bool(0.5) {
        true => y,
        false => -y,
    };
}

struct MainState {
    player_1_pos: na::Point2<f32>,
    player_2_pos: na::Point2<f32>,
    obstacle_right: na::Point2<f32>,
    obstacle_left: na::Point2<f32>,
    ball_pos: na::Point2<f32>,
    ball_vel: na::Vector2<f32>,
    obstacle_right_vel: na::Vector2<f32>,
    obstacle_left_vel: na::Vector2<f32>,
    player_1_score: i32,
    player_2_score: i32,
}

impl MainState {
    pub fn new(ctx: &mut Context) -> Self {
        let (screen_w, screen_h) = graphics::drawable_size(ctx);
        let (screen_w_half, screen_h_half) = (screen_w * 0.5, screen_h * 0.5);

        let mut ball_vel = na::Vector2::new(0.0, 0.0);
        randomize_vec(&mut ball_vel, BALL_SPEED, BALL_SPEED);

        MainState {
            player_1_pos: na::Point2::new(PADDLE_WIDTH_HALF, screen_h_half),
            player_2_pos: na::Point2::new(screen_w-PADDLE_WIDTH_HALF, screen_h_half),
            obstacle_right: na::Point2::new(screen_w_half + 10.0, screen_h_half),
            obstacle_left: na::Point2::new(screen_w_half - 10.0, screen_h_half),
            ball_pos: na::Point2::new(screen_w_half, screen_h_half),
            ball_vel,
            obstacle_right_vel: na::Vector2::new(0.0, -3.0),
            obstacle_left_vel: na::Vector2::new(0.0, 3.0),
            player_1_score: 0,
            player_2_score: 0,
        }
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        let dt = ggez::timer::delta(ctx).as_secs_f32();
        let (screen_w, screen_h) = graphics::drawable_size(ctx);
        move_paddle(&mut self.player_1_pos, KeyCode::Z, -1.0, ctx);
        move_paddle(&mut self.player_1_pos, KeyCode::S, 1.0, ctx);
        move_paddle(&mut self.player_2_pos, KeyCode::Up, -1.0, ctx);
        move_paddle(&mut self.player_2_pos, KeyCode::Down, 1.0, ctx);

        self.ball_pos += self.ball_vel * dt;
        self.obstacle_right += self.obstacle_right_vel;
        self.obstacle_left += self.obstacle_left_vel;

        if self.ball_pos.x < 0.0 {
            self.ball_pos.x = screen_w * 0.5;
            self.ball_pos.y = screen_h * 0.5;
            randomize_vec(&mut self.ball_vel, BALL_SPEED, BALL_SPEED);
            self.player_2_score += 1;
        }
        if self.ball_pos.x > screen_w {
            self.ball_pos.x = screen_w * 0.5;
            self.ball_pos.y = screen_h * 0.5;
            randomize_vec(&mut self.ball_vel, BALL_SPEED, BALL_SPEED);
            self.player_1_score += 1;
        }

        if self.ball_pos.y < BALL_SIZE_HALF {
            self.ball_pos.y = BALL_SIZE_HALF;
            self.ball_vel.y = self.ball_vel.y.abs();
        } else if self.ball_pos.y > screen_h - BALL_SIZE_HALF {
            self.ball_pos.y = screen_h - BALL_SIZE_HALF;
            self.ball_vel.y = -self.ball_vel.y.abs();
        }

        let intersects_player_1 =  
            self.ball_pos.x - BALL_SIZE_HALF < self.player_1_pos.x + PADDLE_WIDTH_HALF
            && self.ball_pos.x + BALL_SIZE_HALF > self.player_1_pos.x - PADDLE_WIDTH_HALF
            && self.ball_pos.y - BALL_SIZE_HALF < self.player_1_pos.y + PADDLE_HEIGHT_HALF
            && self.ball_pos.y + BALL_SIZE_HALF > self.player_1_pos.y - PADDLE_HEIGHT_HALF;
        
        if intersects_player_1 {
            self.ball_vel.x = self.ball_vel.x.abs();
        }

        let intersects_player_2 =  
            self.ball_pos.x - BALL_SIZE_HALF < self.player_2_pos.x + PADDLE_WIDTH_HALF
            && self.ball_pos.x + BALL_SIZE_HALF > self.player_2_pos.x - PADDLE_WIDTH_HALF
            && self.ball_pos.y - BALL_SIZE_HALF < self.player_2_pos.y + PADDLE_HEIGHT_HALF
            && self.ball_pos.y + BALL_SIZE_HALF > self.player_2_pos.y - PADDLE_HEIGHT_HALF;
        
        if intersects_player_2 {
            self.ball_vel.x = -self.ball_vel.x.abs();
        }

        let intersects_wall_right =  
            self.ball_pos.x - BALL_SIZE_HALF < self.obstacle_right.x + PADDLE_WIDTH_HALF
            && self.ball_pos.x + BALL_SIZE_HALF > self.obstacle_right.x - PADDLE_WIDTH_HALF
            && self.ball_pos.y - BALL_SIZE_HALF < self.obstacle_right.y + PADDLE_HEIGHT_HALF
            && self.ball_pos.y + BALL_SIZE_HALF > self.obstacle_right.y - PADDLE_HEIGHT_HALF;
        
        if intersects_wall_right {
            self.ball_vel.x = self.ball_vel.x.abs();
        }

        let intersects_wall_left =  
            self.ball_pos.x - BALL_SIZE_HALF < self.obstacle_left.x + PADDLE_WIDTH_HALF
            && self.ball_pos.x + BALL_SIZE_HALF > self.obstacle_left.x - PADDLE_WIDTH_HALF
            && self.ball_pos.y - BALL_SIZE_HALF < self.obstacle_left.y + PADDLE_HEIGHT_HALF
            && self.ball_pos.y + BALL_SIZE_HALF > self.obstacle_left.y - PADDLE_HEIGHT_HALF;
        
        if intersects_wall_left {
            self.ball_vel.x = -self.ball_vel.x.abs();
        }

        if self.obstacle_right.y < PADDLE_HEIGHT_HALF {
            self.obstacle_right_vel.y = self.obstacle_right_vel.y.abs();
        } else if self.obstacle_right.y > screen_h - PADDLE_HEIGHT_HALF {
            self.obstacle_right_vel.y = -self.obstacle_right_vel.y.abs();
        }

        if self.obstacle_left.y < PADDLE_HEIGHT_HALF {
            self.obstacle_left_vel.y = self.obstacle_left_vel.y.abs();
        } else if self.obstacle_left.y > screen_h - PADDLE_HEIGHT_HALF {
            self.obstacle_left_vel.y = -self.obstacle_left_vel.y.abs();
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, graphics::BLACK);

        let paddle = graphics::Rect::new(-PADDLE_WIDTH_HALF, -PADDLE_HEIGHT_HALF, PADDLE_WIDTH, PADDLE_HEIGHT);
        let paddle_mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), paddle,graphics::Color {r: 0.0, g: 0.0, b: 1.0, a: 1.0})?;
        
        let screen_h = graphics::drawable_size(ctx).1;
        
        
        let ball_rect = graphics::Rect::new(-BALL_SIZE_HALF, -BALL_SIZE_HALF, BALL_SIZE, BALL_SIZE);
        let ball_mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), ball_rect, graphics::Color {r: 1.0, g: 0.5, b: 0.0, a : 1.0})?;
        
        let middle_rect = graphics::Rect::new(-MIDDLE_LINE_W * 0.5, 0.0, MIDDLE_LINE_W, screen_h);
        let middle_mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), middle_rect, graphics::WHITE)?;
        
        let wall_right_rect = graphics::Rect::new(-PADDLE_WIDTH_HALF * 0.5, -PADDLE_HEIGHT_HALF, PADDLE_WIDTH * 0.5, PADDLE_HEIGHT);
        let wall_right_mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), wall_right_rect, graphics::Color {r: 1.0, g: 0.0, b: 0.0, a: 1.0})?;

        let wall_left_rect = graphics::Rect::new(-PADDLE_WIDTH_HALF * 0.5, -PADDLE_HEIGHT_HALF, PADDLE_WIDTH * 0.5, PADDLE_HEIGHT);
        let wall_left_mesh = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), wall_left_rect, graphics::Color {r: 0.0, g: 1.0, b: 0.0, a: 1.0})?;

        let mut draw_param = graphics::DrawParam::default();

        let screen_middle_x = graphics::drawable_size(ctx).0* 0.5;

        draw_param.dest = [screen_middle_x, 0.0].into();
        graphics::draw(ctx, &middle_mesh, draw_param)?;
        
        draw_param.dest = self.obstacle_right.into();
        graphics::draw(ctx, &wall_right_mesh, draw_param)?;
        draw_param.dest = self.obstacle_left.into();
        graphics::draw(ctx, &wall_left_mesh, draw_param)?;

        draw_param.dest = self.player_1_pos.into();
        graphics::draw(ctx, &paddle_mesh, draw_param)?;

        draw_param.dest = self.player_2_pos.into();
        graphics::draw(ctx, &paddle_mesh, draw_param)?;

        draw_param.dest = self.ball_pos.into();
        graphics::draw(ctx, &ball_mesh, draw_param)?;

        let score_text = graphics::Text::new(format!("{}       {}", self.player_1_score, self.player_2_score));
        let screen_w = graphics::drawable_size(ctx).0;
        let screen_w_half = screen_w * 0.5;
        
        let mut score_pos = na::Point2::new(screen_w_half, 40.0);
        let (score_text_w, score_text_h) = score_text.dimensions(ctx);
        score_pos -= na::Vector2::new(score_text_w as f32 * 0.5, score_text_h as f32 * 0.5);
        draw_param.dest = score_pos.into();
        

        graphics::draw(ctx, &score_text, draw_param)?;

        graphics::present(ctx)?;
        Ok(())
    }
}

fn main() -> GameResult {
    let cb = ggez::ContextBuilder::new("Pong", "Elias");
    let (mut ctx, mut event_loop) = cb.build()?;
    graphics::set_window_title(&ctx, "PONG");

    let mut state = MainState::new(&mut ctx);
    event::run(&mut ctx, &mut event_loop, &mut state);
    Ok(())
}
