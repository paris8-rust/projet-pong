__Le Pong__

Notre projet consistera à créer le jeu nommé Pong !!

- Créer une fenêtre avec un fond noir
- Créer deux paddles (un sur l'extrémité droite, un sur l'extrémité gauche)
- Donner une couleur à chacun des paddles
- Faire en sorte que les paddles puissent renvoyer la balle
- Créer une balle au milieu
- Pouvoir mettre les paddles et la balle en mouvement
- Créer une ligne verticale qui sépare la fenêtre en deux afin d'avoir deux camps
- Faire en sorte de comptabiliser les points et d'afficher le score
